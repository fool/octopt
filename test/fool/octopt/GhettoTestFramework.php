<?php

trait GhettoTestFramework
{
    public function assertTrue($expression, $message)
    {
        if ($expression !== true) {
            echo $message, PHP_EOL;
            exit(1);
        }
    }

    public function assertFalse($expression, $message)
    {
        if ($expression !== false) {
            echo $message, PHP_EOL;
            exit(1);
        }
    }

    public function assertEquals($expected, $seen, $message)
    {
        if ($expected !== $seen) {
            echo $message, PHP_EOL;
            exit(1);
        }
    }
}
