<?php

use fool\octopt\Arg;
use fool\octopt\Flag;
use fool\octopt\MultiValue;
use fool\octopt\OptionParser;
use fool\octopt\Positional;
use fool\octopt\Value;

require "src/fool/octopt/Option.php";
require "src/fool/octopt/Positional.php";
require "src/fool/octopt/Flag.php";
require "src/fool/octopt/Value.php";
require "src/fool/octopt/MultiValue.php";
require "src/fool/octopt/OptionParser.php";
require "src/fool/octopt/DuplicateOptionException.php";
require "test/fool/octopt/GhettoTestFramework.php";

/**
 * The tests for this code need to do a lot of php invocations so it's
 * a little easier to set it up this way than try to force it in
 * something like phpunit.
 */
class OctoptTest
{
    use GhettoTestFramework;

    public function run(array $argv)
    {
        $testCaseName = $argv[2];
        $method = "test_$testCaseName";

        if (!method_exists($this, $method)) {
            echo "Invalid test: $testCaseName", PHP_EOL;
            exit(2);
        }

        $value = new Value('', 'testcase');
        $this->$method($value);
    }

    public function test_noOptions(Value $value)
    {
        $optionParser = new OptionParser();
        $result = $optionParser->parse($value);
        $this->assertTrue($result, "Parsing failed with no options");
    }

    public function test_flagShort(Value $value)
    {
        $flag = new Flag("a", "");
        $optionParser = new OptionParser(array($value, $flag));
        $result = $optionParser->parse();
        $this->assertTrue($result, "Parsing failed with one short flag");
        $this->assertTrue($flag->hasValue(), "Flag value was not found");
        $this->assertEquals(1, $flag->getValue(), "Flag value is not 1");
    }


    public function test_flagLong(Value $value)
    {
        $flag = new Flag("", "flag");
        $optionParser = new OptionParser(array($value, $flag));
        $result = $optionParser->parse();
        $this->assertTrue($result, "Parsing failed with one long flag");
        $this->assertTrue($flag->hasValue(), "Flag value was not found");
        $this->assertEquals(1, $flag->getValue(), "Flag value is not 1");
    }

    public function test_flagShortAndLong(Value $value)
    {
        $flag = new Flag("f", "flag");
        $optionParser = new OptionParser(array($value, $flag));
        $result = $optionParser->parse();
        $this->assertTrue($result, "Parsing failed with a short flag and long flag combined");
        $this->assertTrue($flag->hasValue(), "Flag value was not found");
        $this->assertEquals(2, $flag->getValue(), "Flag value is not 2");
    }


    public function test_multiFlag(Value $value)
    {
        $flag = new Flag("a", "");
        $optionParser = new OptionParser(array($value, $flag));
        $result = $optionParser->parse();
        $this->assertTrue($result, "Parsing failed with a repeated flag");
        $this->assertTrue($flag->hasValue(), "Flag value was not found");
        $this->assertEquals(2, $flag->getValue(), "Flag value is not 2");
    }


    public function test_multiFlagNoRepeat(Value $value)
    {
        $flagA = new Flag("a", "");
        $flagB = new Flag("b", "");
        $options = array($value, $flagA, $flagB);
        $optionParser = new OptionParser($options);
        $result = $optionParser->parse();
        $this->assertTrue($result, "Parsing failed with multiple flags");

        array_shift($options);
        foreach ($options as $flag) {
            $this->assertTrue($flag->hasValue(), "Flag value was not found");
            $this->assertEquals(1, $flag->getValue(), "Flag value is not 1");
        }
    }


    public function test_valueShort(Value $value)
    {
        $valueCar = new Value("c", "");
        $optionParser = new OptionParser(array($value, $valueCar));
        $result = $optionParser->parse();
        $this->assertTrue($result, "Parsing failed with a value using the short name");
        $this->assertTrue($valueCar->hasValue(), "Value was not found");
        $this->assertEquals('volvo', $valueCar->getValue(), "Value found is not expected");
    }


    public function test_valueLong(Value $value)
    {
        $valueCar = new Value("", "car");
        $optionParser = new OptionParser(array($value, $valueCar));
        $result = $optionParser->parse();
        $this->assertTrue($result, "Parsing failed with a value using the long name");
        $this->assertTrue($valueCar->hasValue(), "Value was not found");
        $this->assertEquals('volvo', $valueCar->getValue(), "Value found is not expected");
    }



    public function test_valueShortAndLong(Value $value)
    {
        $valueCar = new Value("c", "car");
        $optionParser = new OptionParser(array($value, $valueCar));
        $result = $optionParser->parse();
        $this->assertTrue($result, "Parsing failed with a short and long value");
        $this->assertTrue($valueCar->hasValue(), "Value was not found");
        $this->assertEquals('toyota', $valueCar->getValue(), "Value found is not expected");
    }


    public function test_values(Value $value)
    {
        $valueCar = new Value("c", "car");
        $valuePlane = new Value("p", "plane");
        $options = array($value, $valueCar, $valuePlane);
        $optionParser = new OptionParser($options);
        $result = $optionParser->parse();
        $this->assertTrue($result, "Parsing failed with multiple values");

        $this->assertTrue($valueCar->hasValue(), "Car value was not found");
        $this->assertEquals('volvo', $valueCar->getValue(), "Car value found is not expected");
        $this->assertTrue($valuePlane->hasValue(), "Plane value was not found");
        $this->assertEquals('boeing', $valuePlane->getValue(), "Plane value found is not expected");
    }


    public function test_valuesNoneWhenOptional(Value $value)
    {
        $valueCar = new Value("c", "car", false);
        $optionParser = new OptionParser(array($value, $valueCar));
        $result = $optionParser->parse();
        $this->assertTrue($result, "Parsing failed with no options");
        $this->assertFalse($valueCar->hasValue(), "Value was found");
    }


    public function test_multiValueSingle(Value $value)
    {
        $valueCar = new MultiValue("c", "car");
        $optionParser = new OptionParser(array($value, $valueCar));
        $result = $optionParser->parse();
        $this->assertTrue($result, "Parsing failed with multivalue");
        $this->assertTrue($valueCar->hasValue(), "Value was found");
        $this->assertEquals(array('volvo'), $valueCar->getValue(), "Value found is not expected");
    }


    public function test_multiValuesMany(Value $value)
    {
        $valueCar = new MultiValue("c", "car");
        $optionParser = new OptionParser(array($value, $valueCar));
        $result = $optionParser->parse();
        $this->assertTrue($result, "Parsing failed with multiple multivalues");
        $this->assertTrue($valueCar->hasValue(), "Value was found");
        $this->assertEquals(array('volvo', 'toyota', 'honda'), $valueCar->getValue(), "Value found is not expected");
    }

    public function test_multiValuesNoneWhenOptional(Value $value)
    {
        $valueCar = new MultiValue("c", "car", false);
        $optionParser = new OptionParser(array($value, $valueCar));
        $result = $optionParser->parse();
        $this->assertTrue($result, "Parsing failed with no options");
        $this->assertFalse($valueCar->hasValue(), "Value was found");
    }


    public function test_positionalWhenNone(Value $value)
    {
        $valueCar = new Positional(0, false);
        $optionParser = new OptionParser(array($value, $valueCar));
        $result = $optionParser->parse();
        $this->assertTrue($result, "Parsing failed with no options");
        $this->assertFalse($valueCar->hasValue(), "Value was found");
    }

    public function test_positional(Value $value)
    {
        $hello = new Positional(0);
        $optionParser = new OptionParser(array($value, $hello));
        $result = $optionParser->parse();
        $this->assertTrue($result, "Parsing failed with one positional argument");
        $this->assertTrue($hello->hasValue(), "Value hello was not found");
        $this->assertEquals('hello', $hello->getValue(), "Value was incorrect: {$hello->getValue()} ");
    }

    public function test_positionalMultiple(Value $value)
    {
        $red = new Positional(0);
        $stripe = new Positional(1);
        $hooray = new Positional(2);
        $beer = new Positional(3);
        $optionParser = new OptionParser(array($value, $red, $stripe, $hooray, $beer));
        $result = $optionParser->parse();
        $this->assertTrue($result, "Parsing failed with multiple positional arguments");
        $this->assertTrue($red->hasValue(), "Value red was not found");
        $this->assertEquals('red', $red->getValue(), "Value was incorrect: {$red->getValue()} ");
        $this->assertTrue($stripe->hasValue(), "Value stripe was not found");
        $this->assertEquals('stripe', $stripe->getValue(), "Value was incorrect: {$stripe->getValue()} ");
        $this->assertTrue($hooray->hasValue(), "Value hooray was not found");
        $this->assertEquals('hooray', $hooray->getValue(), "Value was incorrect: {$hooray->getValue()} ");
        $this->assertTrue($beer->hasValue(), "Value beer was not found");
        $this->assertEquals('beer', $beer->getValue(), "Value was incorrect: {$beer->getValue()} ");
    }

    public function test_positionalFlag(Value $value)
    {
        $hello = new Positional(0);
        $hi = new Flag("h", "");
        $goodbye = new Positional(1);
        $optionParser = new OptionParser(array($value, $hello, $hi, $goodbye));
        $result = $optionParser->parse();
        $this->assertTrue($result, "Parsing failed with one positional with flag");
        $this->assertTrue($hello->hasValue(), "Value hello was not found");
        $this->assertEquals('hello', $hello->getValue(), "Value was incorrect: {$hello->getValue()} ");
        $this->assertTrue($hi->hasValue(), "Value hi was not found");
        $this->assertTrue($hi->getValue() === 1, "Value was incorrect: {$hi->getValue()} ");
        $this->assertTrue($goodbye->hasValue(), "Value goodbye was not found");
        $this->assertEquals('goodbye', $goodbye->getValue(), "Goodbye value was incorrect: {$goodbye->getValue()} ");
    }


    public function test_positionalValue(Value $value)
    {
        $mars = new Positional(0);
        $yes = new Flag("y", "yes");
        $yep = new Flag("", "yep");
        $venus = new Positional(1);
        $earth = new Positional(2);
        $mercury = new Positional(3);
        $optionParser = new OptionParser(array($value, $mars, $yes, $yep, $venus, $earth, $mercury));
        $result = $optionParser->parse();
        $this->assertTrue($result, "Parsing failed with one positional with flag");
        $this->assertTrue($mars->hasValue(), "Value mars was not found");
        $this->assertEquals('mars', $mars->getValue(), "Value mars was incorrect: {$mars->getValue()} ");
        $this->assertTrue($yes->hasValue(), "Value yes was not found");
        $this->assertTrue($yes->getValue() === 3, "Value yes was incorrect: {$yes->getValue()} ");
        $this->assertTrue($yep->hasValue(), "Value yep was not found");
        $this->assertTrue($yep->getValue() === 1, "Value yep was incorrect: {$yep->getValue()} ");
        $this->assertTrue($venus->hasValue(), "Value venus was not found");
        $this->assertEquals('venus', $venus->getValue(), "Value venus was incorrect: {$venus->getValue()} ");
        $this->assertTrue($earth->hasValue(), "Value earth was not found");
        $this->assertEquals('earth', $earth->getValue(), "Value earth was incorrect: {$earth->getValue()} ");
        $this->assertTrue($mercury->hasValue(), "Value mercury was not found");
        $this->assertEquals('mercury', $mercury->getValue(), "Value mercury was incorrect: {$mercury->getValue()} ");
    }


    public function test_positionalMultiValue(Value $value)
    {
        $mars = new Positional(0);
        $v = new MultiValue("v", "");
        $earth = new Positional(1);
        $optionParser = new OptionParser(array($value, $mars, $v, $earth));
        $result = $optionParser->parse();
        $this->assertTrue($result, "Parsing failed with one positional with flag");
        $this->assertTrue($mars->hasValue(), "Value mars was not found");
        $this->assertEquals('mars', $mars->getValue(), "Value mars was incorrect: {$mars->getValue()} ");
        $this->assertTrue($v->hasValue(), "Value yes was not found");
        $this->assertEquals(array('venus', 'volleyball'), $v->getValue(), "Value v was incorrect");
        $this->assertTrue($earth->hasValue(), "Value earth was not found");
        $this->assertEquals('earth', $earth->getValue(), "Value earth was incorrect: {$earth->getValue()} ");
    }



    public function test_many1(Value $value)
    {
        $burger = new MultiValue("", "burger");
        $fries = new Flag("", "fries");
        $drink = new Value("", "drink");

        $optionParser = new OptionParser(array($value, $burger, $fries, $drink));
        $result = $optionParser->parse();
        $this->assertTrue($result, "Parsing failed");
        $this->assertTrue($burger->hasValue(), "Burger value was found");
        $this->assertEquals(array('mcdonalds', 'in-n-out'), $burger->getValue(), "Burger value found is not expected");
        $this->assertTrue($fries->hasValue(), "Fries value was found");
        $this->assertEquals(1, $fries->getValue(), "Fries value found is not 1");
        $this->assertTrue($drink->hasValue(), "Drink value was found");
        $this->assertEquals('rootbeer', $drink->getValue(), "Drink value found is not expected");
    }

    public function test_many2(Value $value)
    {
        $a = new Flag('a', '');
        $b = new Flag('b', '');
        $c = new MultiValue('c', '');
        $pos1 = new Positional(0);
        $pos2 = new Positional(1);
        $d = new Flag('d', '');
        $e = new Value('e', '');
        $pos3 = new Positional(2);
        $pos4 = new Positional(3);
        $f = new Value('f', '');
        $pos5 = new Positional(4);

        $optionParser = new OptionParser(array($value, $a, $b, $c, $d, $e, $f, $pos1, $pos2, $pos3, $pos4, $pos5));
        $result = $optionParser->parse();
        $this->assertTrue($result, "Parsing failed");
        $this->assertTrue($a->hasValue(), "Value a was not found");
        $this->assertTrue($a->getValue() === 4, "Value a was incorrect: {$a->getValue()} ");
        $this->assertTrue($b->hasValue(), "Value b was not found");
        $this->assertTrue($b->getValue() === 1, "Value b was incorrect: {$b->getValue()} ");
        $this->assertTrue($c->hasValue(), "Value c was not found");
        $this->assertEquals(array('1', '2', '3'), $c->getValue(), "Value c was incorrect");
        $this->assertTrue($d->hasValue(), "Value d was not found");
        $this->assertTrue($d->getValue() === 1, "Value d was incorrect: {$d->getValue()} ");
        $this->assertTrue($e->hasValue(), "Value e was not found");
        $this->assertEquals('eVal', $e->getValue(), "Value e was incorrect: {$e->getValue()} ");
        $this->assertTrue($f->hasValue(), "Value f was not found");
        $this->assertEquals('fVal', $f->getValue(), "Value f was incorrect: {$f->getValue()} ");
        $this->assertTrue($pos1->hasValue(), "Value pos1 was not found");
        $this->assertEquals('pos1', $pos1->getValue(), "Value pos1 was incorrect: {$pos1->getValue()} ");
        $this->assertTrue($pos2->hasValue(), "Value pos2 was not found");
        $this->assertEquals('pos2', $pos2->getValue(), "Value pos2 was incorrect: {$pos2->getValue()} ");
        $this->assertTrue($pos3->hasValue(), "Value pos3 was not found");
        $this->assertEquals('pos3', $pos3->getValue(), "Value pos3 was incorrect: {$pos3->getValue()} ");
        $this->assertTrue($pos4->hasValue(), "Value pos4 was not found");
        $this->assertEquals('pos4', $pos4->getValue(), "Value pos4 was incorrect: {$pos4->getValue()} ");
        $this->assertTrue($pos5->hasValue(), "Value pos5 was not found");
        $this->assertEquals('pos5', $pos5->getValue(), "Value pos5 was incorrect: {$pos5->getValue()} ");
    }

    public function test_extraOptions(Value $value)
    {
        $f = new Flag('f', '');
        $optionParser = new OptionParser(array($value, $f));
        $result = $optionParser->parse();
        $this->assertTrue($result, "Parsing failed");

        $extraOptions = $optionParser->getExtraOptions();
        $this->assertTrue($f->hasValue(), "f does not have a value");
        $this->assertEquals(2, $f->getValue(), "Value f is wrong: {$f->getValue()}");


        $extraName = new Arg('name', Arg::VALUE, 'me');
        $extraH = new Arg('h', Arg::VALUE, 'hellovalue'); // single '-' converts to 1 character flag
        $extraPositional = new Arg('positional2', Arg::POSITIONAL, 0);
        $expectedExtraOptions = array($extraName, $extraH, $extraPositional);

        foreach ($extraOptions as $extra) {
            foreach ($expectedExtraOptions as $i => $expected) {
                if ($extra->equals($expected)) {
                    unset($expectedExtraOptions[$i]);
                    break;
                }
            }
        }
        $this->assertEquals(count($expectedExtraOptions), 0, "Unable to find all the extra options");
    }

    public function test_extraOptions2(Value $value)
    {
        $pos3 = new Positional(2);
        $pos4 = new Positional(3);
        $optionParser = new OptionParser(array($value, $pos3, $pos4));
        $result = $optionParser->parse();
        $this->assertTrue($result, "Parsing failed");

        $this->assertTrue($pos3->hasValue(), "pos3 does not have a value");
        $this->assertEquals('pos3', $pos3->getValue(), "Value pos3 is wrong: {$pos3->getValue()}");
        $this->assertTrue($pos4->hasValue(), "pos4 does not have a value");
        $this->assertEquals('pos4', $pos4->getValue(), "Value pos4 is wrong: {$pos4->getValue()}");
        $extraOptions = $optionParser->getExtraOptions();

        $extraPos1 = new Arg('pos1', Arg::POSITIONAL, 0);
        $extraPos2 = new Arg('pos2', Arg::POSITIONAL, 1);
        $extraV = new Arg('v', Arg::VALUE, 'val');
        $extraF = new Arg('f', Arg::FLAG);
        $expectedExtraOptions = array($extraPos1, $extraPos2, $extraV, $extraF);

        foreach ($extraOptions as $extra) {
            foreach ($expectedExtraOptions as $i => $expected) {
                if ($extra->equals($expected)) {
                    unset($expectedExtraOptions[$i]);
                    break;
                }
            }
        }
        $this->assertEquals(count($expectedExtraOptions), 0, "Unable to find all the extra options");
    }

    public function test_missingOptions(Value $value)
    {
        $mars = new Positional(0);
        $yes = new Flag("y", "yes");
        $yep = new Flag("", "yep", false);
        $earth = new Positional(1);
        $mercury = new Positional(2);
        $optionParser = new OptionParser(array($value, $mars, $yes, $yep, $earth, $mercury));
        $result = $optionParser->parse();
        $this->assertTrue($result, "Parsing failed with missing options");
        $this->assertTrue($mars->hasValue(), "Value mars was not found");
        $this->assertEquals('mars', $mars->getValue(), "Value mars was incorrect: {$mars->getValue()} ");
        $this->assertTrue($yes->hasValue(), "Value yes was not found");
        $this->assertTrue($yes->getValue() === 2, "Value yes was incorrect: {$yes->getValue()} ");
        $this->assertFalse($yep->hasValue(), "Value yep was found");
        $this->assertTrue($earth->hasValue(), "Value earth was not found");
        $this->assertEquals('earth', $earth->getValue(), "Value earth was incorrect: {$earth->getValue()} ");
        $this->assertTrue($mercury->hasValue(), "Value mercury was not found");
        $this->assertEquals('mercury', $mercury->getValue(), "Value mercury was incorrect: {$mercury->getValue()} ");

        $missingOptions = $optionParser->getMissingOptions();
        $this->assertEquals(1, count($missingOptions), "Too many options missing");
        $this->assertEquals($yep, $missingOptions[0], "Incorrect missing option");
    }
}

$test = new OctoptTest();
$test->run($argv);
