<?php

namespace fool\octopt;

/**
 * A multivalue is a value that can appear any number of times.
 *
 * Example:
 *
 *   tail -f file1 -f file2 -f file3
 *
 * "f" is a multi value and the result would be
 *
 * array('file1', 'file2', 'file3')
 */
class MultiValue extends Value
{
    /**
     * Multivalues should always give you an array back even if there was only 1
     *
     * @param array|string $value
     */
    public function setValue($value)
    {
        if (!is_array($value)) {
            $value = array($value);
        }
        $this->value = $value;
    }
}
