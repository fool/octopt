<?php

namespace fool\octopt;

/**
 * A value can appear only once and is separated by a space.
 *
 * program --filename file.txt
 *
 * "filename" is a value, and the value is "file.txt".
 */
class Value extends Option
{
    /**
     * @return string
     */
    public function getShortGetOpt()
    {
        if ($this->shortName) {
            return $this->shortName . ':';
        }
        return '';
    }

    /**
     * @return string
     */
    public function getLongGetOpt()
    {
        if ($this->longName) {
            return $this->longName . ':';
        }
        return '';
    }

    /**
     * Single values are only allowed to be there once, if there's an array then this failed.
     *
     * @param array|string $value
     */
    public function setValue($value)
    {
        if (is_string($value)) {
            $this->value = $value;
        } elseif (is_array($value)) {
            /* take the first value entered */
            $this->value = $value[0];
        }
    }
}
