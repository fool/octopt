<?php

namespace fool\octopt;

/**
 * Abstracts out option parsing
 */
class OptionParser
{
    const STATE_POSITIONAL = 1;
    const STATE_OPTION = 2;

    /**
     * All the options searched for
     *
     * @var Option[]
     */
    private $options;

    /**
     * The options that were not found.
     *
     * @var Option[]
     */
    private $missingOptions;

    /**
     * CLI args that were not described by any of the input criteria.
     *
     * @var Arg[]
     */
    private $extraOptions;

    /**
     * @param Option[] $options
     */
    public function __construct(array $options = array())
    {
        $this->options = array();
        foreach ($options as $option) {
            $this->addOption($option);
        }
    }

    /**
     * @param Option $option
     */
    public function addOption(Option $option)
    {
        $this->options[] = $option;
    }

    /**
     * @return bool
     */
    public function parse()
    {
        return $this->getOptions($_SERVER['argv']);
    }

    /**
     * @param  Arg[] $tokens
     * @return bool
     */
    protected function hasValidNames(array $tokens)
    {
        /** check for invalid naming $nameMap */
        $nameMap = array();
        foreach ($tokens as $token) {
            $type = $token->getType();
            if ($type === Arg::POSITIONAL) {
                continue;
            }

            $name = $token->getName();
            if (isset($nameMap[$name])) {
                if ($nameMap[$name] !== $type) {
                    // you cant declare the same name for multiple types
                    // -j hello -j -j -j -j hi.....    positional doesnt count
                    return false;
                } else {
                    $nameMap[$name] = $type;
                }
            }
        }
        return true;
    }

    /**
     * option parsing.
     *
     * returns true if all required options were found.
     *
     *
     * @param  string[] $argv
     * @return bool     True if all required options were found, False otherwise
     */
    public function getOptions(array $argv)
    {
        /* ignore program name */
        array_shift($argv);

        $this->missingOptions = array();
        $tokens = $this->tokenizeArguments($argv);
        if (!$this->hasValidNames($tokens)) {
            return false;
        }

        $tokensRemaining = $tokens;
        foreach ($this->options as $option) {
            $found = false;
            if ($option instanceof Positional) {

                foreach ($tokensRemaining as $i => $token) {

                    if ($token->getType() === Arg::POSITIONAL
                        && $token->getValue() === $option->getIndex()) {
                        $option->setValue($token->getName());
                        $found = true;
                        unset($tokensRemaining[$i]);
                        break;
                    }
                }
            } else {
                $shortName = $option->getShortName();
                $longName = $option->getLongName();

                $isFlag = $option instanceof Flag;

                /**
                 * Value starts off 0 for the flag counter.
                 * Each flag seen increments value by 1
                 *
                 * In value mode the first value is assigned to $value
                 * The second sighting changes $value to an array of the original and 2nd value
                 * Subsequent sightings are appended to the array
                 * This matches php getopt()
                 */
                $value = 0;
                foreach ($tokensRemaining as $i => $token) {
                    if ($token->getName() === $shortName
                        || $token->getName() === $longName) {
                        if ($isFlag) {
                            $value += 1;
                        } else {
                            if ($value === 0) {
                                $value = $token->getValue();
                            } elseif (!is_array($value)) {
                                $value = array($value, $token->getValue());
                            } else {
                                $value[] = $token->getValue();
                            }
                        }
                        $found = true;
                        unset($tokensRemaining[$i]);
                    }
                }
                if ($found) {
                    $option->setValue($value);
                }
            }

            if (!$found) {
                $this->missingOptions[] = $option;
            }
        }

        if (count($tokensRemaining) > 0) {
            $this->extraOptions = $tokensRemaining;
        }


        return $this->hasAllRequiredOptions();
    }

    /**
     * @return bool
     */
    public function hasAllRequiredOptions()
    {
        $hasAllOptions = true;
        foreach ($this->missingOptions as $missingOption) {
            if ($missingOption->isRequired()) {
                $hasAllOptions = false;
                break;
            }
        }
        return $hasAllOptions;
    }


    /**
     * Read the command invocation and tokenize into a list of arguments
     *
     * @param  string[] $argv
     * @return Arg[]
     */
    public function tokenizeArguments(array $argv)
    {
        $values = array();
        $state = self::STATE_POSITIONAL;
        $name = '';
        $stateTransitions = 0;
        $positionalIndex = 0;
        $length = 0;
        foreach ($argv as $arg)
        {
            $length = strlen($arg);
            /* i dont think this can happen */
            if ($length < 1) {
                continue;
            }

            switch ($state) {
                case self::STATE_POSITIONAL:
                    if ($arg[0] === '-') {
                        $state = self::STATE_OPTION;
                        $name = $this->parseOptionName($arg, $length);
                        if ($this->isFlagName($name)) {
                            $values[] = new Arg($name, Arg::FLAG);
                            $state = self::STATE_POSITIONAL;
                        }
                    } else {
                        $name = $arg;
                        $values[] = new Arg($arg, Arg::POSITIONAL, $positionalIndex);
                        $positionalIndex += 1;
                    }
                    break;

                case self::STATE_OPTION:
                    if ($arg[0] === '-') {
                        $values[] = new Arg($name, Arg::FLAG); /* undeclared flag */
                        $name = $this->parseOptionName($arg, $length);
                        /* state remains option */
                    } else {
                        $values[] = new Arg($name, Arg::VALUE, $arg);
                        $state = self::STATE_POSITIONAL;
                    }
                    break;
            }
            $stateTransitions += 1;
        }

        /**
         * If you end in state OPTION and your arg looks like an option, starts with '-',
         * then you have to add it as a flag.
         */
        if ($state == self::STATE_OPTION        // this only counts in state option
            && $stateTransitions >= 1           // you had to have seen at least one argument
            && $length > 0) {                   // you have some name
            $values[] = new Arg($name, Arg::FLAG);
        }

        return $values;
    }

    /**
     * @param  string  $arg
     * @param  int     $length
     * @return string
     */
    private function parseOptionName($arg, $length)
    {
        if ($length > 1 && $arg[1] === '-') {
            $name = substr($arg, 2);
        } else {
            if ($length === 1) {
                $name = '';
            } else {
                $name = substr($arg, 1, 1);
            }
        }
        return $name;
    }

    /**
     * Only available after calling getOptions
     * @return Option[]
     */
    public function getMissingOptions()
    {
        return $this->missingOptions;
    }

    /**
     * Only available after calling getOptions
     * @return \fool\octopt\Arg[]
     */
    public function getExtraOptions()
    {
        return $this->extraOptions;
    }

    /**
     * @param  string $name
     * @return bool
     */
    protected function isFlagName($name)
    {
        foreach ($this->options as $option) {
            if ($option instanceof Flag
                && ($option->getShortName() === $name || $option->getLongName() === $name))
                return true;
        }
        return false;
    }

}

/**
 * Private class to represent an argument before any contextual
 * clues are applied
 */
class Arg
{
    /**
     * @var string
     */
    private $name;
    /**
     * @var string
     */
    private $type;
    /**
     * @var string
     */
    private $value;

    const POSITIONAL = 'positional';
    const FLAG = 'flag';
    const VALUE = 'value';

    /**
     * @param string $name
     * @param string $type
     * @param string $value
     */
    function __construct($name, $type, $value = '')
    {
        $this->name = $name;
        $this->type = $type;
        $this->value = $value;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @return string
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @param  Arg $other
     * @return bool
     */
    public function equals(Arg $other)
    {
        return $this->name === $other->getName()
               && $this->type === $other->getType()
               && $this->value === $other->getValue();
    }
}
