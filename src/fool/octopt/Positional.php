<?php

namespace fool\octopt;

/**
 * Positional arguments are any non-option args sitting on the command line.
 *
 * program -allow all username password
 *
 *
 * "allow" is a value; value: all
 * "username" is a Positional with shortname '' and longname '0'; value: username
 * "password" is a Positional with shortname '' and longname '1'; value: password
 */
class Positional extends Option
{
    public function __construct($index, $required = true)
    {
        parent::__construct('', $index, $required);
    }

    public function getIndex()
    {
        return $this->longName;
    }
} 