<?php

namespace fool\octopt;

/**
 * Base of all the option types.
 */
abstract class Option
{
    /**
     * @var string
     */
    protected $shortName;

    /**
     * @var string
     */
    protected $longName;

    /**
     * @var array|string|null
     */
    protected $value;

    /**
     * @var bool
     */
    protected $required;

    /**
     * @param string $longName
     * @param string $shortName
     * @param bool   $required
     */
    public function __construct($shortName, $longName, $required = true)
    {
        $this->value = null;
        $this->longName = $longName;
        $this->shortName = $shortName;
        $this->required = $required;
    }

    /**
     * @return string
     */
    public function getShortGetOpt()
    {
        return $this->shortName;
    }

    /**
     * @return string
     */
    public function getLongGetOpt()
    {
        return $this->longName;
    }

    /**
     * @return string
     */
    public function getLongName()
    {
        return $this->longName;
    }

    /**
     * @return string
     */
    public function getShortName()
    {
        return $this->shortName;
    }

    /**
     * @param  string|array $value
     */
    public function setValue($value)
    {
        $this->value = $value;
    }

    /**
     * @return string|array|null
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @return bool
     */
    public function hasValue()
    {
        return $this->value !== null;
    }

    /**
     * @return bool
     */
    public function isRequired()
    {
        return $this->required;
    }

    /**
     * @param  mixed $default
     * @return mixed
     */
    public function getValueOrElse($default)
    {
        if ($this->hasValue()) {
            return $this->getValue();
        }
        return $default;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return "(-{$this->shortName}, --{$this->longName})";
    }
}
