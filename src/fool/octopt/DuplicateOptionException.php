<?php

namespace fool\octopt;
use \Exception;

/**
 * Thrown when you try to ask for the same option multiple times
 */
class DuplicateOptionException extends Exception
{
    /**
     * @param string $option    The option that was duplicated
     */
    public function __construct($option)
    {
        parent::__construct("Option '$option' was declared twice");
    }
}
