<?php

namespace fool\octopt;

/**
 * A flag is a single option that exists or not. Example:
 *
 * ls -l
 *
 * "-l" is a flag. There is no value. geValue() on a flag will return true if it exists, null (uninitialized) otherwise
 */
class Flag extends Option
{
    /**
     * @param int $value
     */
    public function setValue($value)
    {
        $this->value = $value;
    }
}
